export default {
  toServer(paginationInfo) {
    return { ...paginationInfo, limit: paginationInfo.itemsPerPage };
  },

  generate({ itemsPerPage = 20, page = 1 } = {}) {
    return { itemsPerPage, page, footer: { itemsPerPageOptions: [5, 10, 20, 50, 100] } };
  },
};
