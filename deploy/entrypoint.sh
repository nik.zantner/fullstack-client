#!/bin/sh

cd /usr/share/nginx/html/js

echo $API_URL

# set the api url in the index.legacy
sed -i 's@##API_URL##@'"$API_URL"'@g' $(ls | grep index | sed -n 1p)

# set the api url in the default index
sed -i 's@##API_URL##@'"$API_URL"'@g' $(ls | grep index | sed -n 3p)


# nginx load
exec nginx -g 'daemon off;'
