## Description

I just wanted to try NestJS and build a bunch of views for stuff I usually use a combination of Nextcloud, Google Sheets, Notion, etc. . And maybe I want to show of a bit :). UX/UI Design by me, as also the engineering and implementation and dev-ops stuff.

## Dev Build

```
npm install
```

### Compiles and hot-reloads for development

Make sure your server is running on localhost:8080

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

## Icons

### Generated

https://favicon.io/favicon-generator/

### PWA Icon generated via

```
npx vue-pwa-asset-generator -a default.png -o vue

```
