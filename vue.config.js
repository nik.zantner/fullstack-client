const { gitDescribeSync } = require("git-describe");

process.env.VUE_APP_GIT_HASH = gitDescribeSync().hash;

process.env.VUE_APP_BUILD_DATE = new Date().toLocaleDateString("de-DE");
process.env.VUE_APP_BUILD_TIME = new Date().toLocaleTimeString("de-DE", {
  timeStyle: "short",
});

module.exports = {
  transpileDependencies: ["vuetify"],
  pages: {
    index: {
      entry: "src/main.js",
      title: "Lynx",
    },
  },
  pwa: {
    name: "Lynx",
    themeColor: "#003042",
    workboxOptions: {
      cleanupOutdatedCaches: true,
      skipWaiting: true,
      clientsClaim: true,
    },
  },
};
