module.exports = {
  root: true,

  env: {
    node: true,
  },

  extends: ["plugin:vue/essential", "@vue/airbnb"],

  parserOptions: {
    parser: "babel-eslint",
  },

  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "warn" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "warn" : "off",
    "max-len": ["error", { code: 180 }],
    "no-unused-vars": "off",
    "arrow-parens": "off",
    "no-shadow": "off",
    "implicit-arrow-linebreak": "off",
    "no-param-reassign": "off",
    "no-nested-ternary": "off",
    "import/no-cycle": "off",
    "no-return-assign": "off",
    "object-curly-newline": "off",
    "no-confusing-arrow": "off",
    "no-plusplus": "off",
    "vue/no-mutating-props": "off",
  },

  extends: ["plugin:vue/essential", "@vue/airbnb", "eslint:recommended", "@vue/prettier"],
};
